package com.example.antivirus;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements View.OnClickListener{
	private final static int REQUEST_TEXT = 0;
	private final static int WC = LinearLayout.LayoutParams.WRAP_CONTENT;
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
//		setContentView(R.layout.activity_main);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		LinearLayout layout = new LinearLayout(this);
		layout.setBackgroundColor(Color.rgb(255,255,255));
		layout.setOrientation(LinearLayout.VERTICAL);
		setContentView(layout);
		
		layout.addView(makeButton("aaaaaa","aaaa"));
		layout.addView(makeButton("bbbbbb","bbbb"));


	}

	private Button makeButton(String text, String tag){
		Button button = new Button(this);
		button.setText(text);
		button.setTag(tag);
		button.setOnClickListener(this);
		button.setLayoutParams(new LinearLayout.LayoutParams(WC,WC));
		return button;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String tag = (String)v.getTag();
		if(tag == "aaaa"){
			
		}else if(tag == "bbbb"){
			Intent intent = new Intent(this,com.example.antivirus.PracticeActivity.class);
			startActivityForResult(intent,REQUEST_TEXT);
		}
		
	}

}
