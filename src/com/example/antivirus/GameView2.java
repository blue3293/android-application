package com.example.antivirus;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.app.Activity;
import android.app.AlertDialog;

public class GameView2 extends SurfaceView implements SurfaceHolder.Callback,Runnable,
		GestureDetector.OnDoubleTapListener, OnGestureListener{
	Context myContext;
	private GestureDetector gestureDetector;
	private final static int S_STANDBY=0,
			S_PLAY = 1,
			S_CLEAR = 2,
			S_GAMEOVER = 3;

	private Stage stage;
	
	private SurfaceHolder holder;
	private Thread thread;
	private Bitmap[] bmp = new Bitmap[6];
	
	private int scene = S_STANDBY;
	private int init = S_STANDBY;
	private String message = null;
	private String level = "normal";
	private int speed;
	
	public GameView2(Context context , String select) {
		super(context);
		myContext = context;
		// TODO Auto-generated constructor stub
		bmp[0] = readBitmap(context, "test2");
		bmp[1] = readBitmap(context, "jb_4");
		bmp[2] = readBitmap(context, "jb_6");
		bmp[3] = readBitmap(context, "jb_7");
		bmp[4] = readBitmap(context, "jb_8");
		bmp[5] = readBitmap(context, "jb_5");
		
		
//		bmp[1] = readBitmap(context, "majikichi11");
//		bmp[2] = readBitmap(context, "majikichi51");
//		bmp[3] = readBitmap(context, "majikichi61");
//		bmp[4] = readBitmap(context, "majikichi71");
//		bmp[5] = readBitmap(context, "majikichi41");
		
//		Log.i("GV", select);
		level = select;
		if(level.equals("easy"))	speed = 500;
		else if(level.equals("hard"))	speed = 100;
		else	speed = 300;
		
		stage = new Stage();
		gestureDetector = new GestureDetector(context,this);
		holder = getHolder();
		holder.addCallback(this);
		holder.setFixedSize(480, 800);
	}

	private Bitmap readBitmap(Context context, String name) {
		// TODO Auto-generated method stub
		int resID = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
		return BitmapFactory.decodeResource(context.getResources(), resID);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
//		Canvas canvas;
//		Paint paint = new Paint(); 
		
		while(thread != null){
			if(init >= 0){
				scene = init;
				init = -1;
				if(scene == S_STANDBY){
					message = "Touch Start";
				}else if (scene == S_PLAY){
					message = null;
				}else if (scene == S_CLEAR){
					message = "Clear";
				}else if(scene == S_GAMEOVER){
					message = "Game Over";
				}
			}
			if (scene == S_PLAY){
				if(stage.getResult()){
					stage.step();
					stage.puyoDelete();
				}else
					init = S_GAMEOVER;
			}
			drawCanvas();
			try{
				Thread.sleep(speed);
			}catch(Exception e){
				
			}
		}
	}

	private void drawCanvas(){
		Canvas canvas;
		Paint paint = new Paint(); 
		canvas = holder.lockCanvas();
		canvas.drawBitmap(bmp[0], 0, 0, null);
//		canvas.drawColor(Color.WHITE);
		
		paint.setColor(Color.WHITE);
//		paint.setColor(Color.BLACK);
		paint.setTextSize(30);
		canvas.drawText("SCORE:" + num2str(stage.getScore(),4), 10, 40, paint);
		
		drawIcon(canvas);
		
		if(message != null){
			canvas.drawText(message, (480-paint.measureText(message))/2, 450, paint);
		}
		holder.unlockCanvasAndPost(canvas);
	}
	
	private void drawIcon(Canvas canvas){
		for(int i=0;i<6;i++)
			for(int j=1;j<13;j++){
				int col = stage.getField()[i][j];
				if(col == 1){
					if(!level.equals("extra"))
						col = stage.getColor(i,j);
					else
						canvas.drawBitmap(bmp[5],i*80,(j-1)*60 + 62, null);
				}
				if(col == 2){
					canvas.drawBitmap(bmp[1],i*80,(j-1)*60 + 62, null);
				}else if(col == 3){
					canvas.drawBitmap(bmp[2],i*80,(j-1)*60 + 62, null);
				}else if(col == 4){
					canvas.drawBitmap(bmp[3],i*80,(j-1)*60 + 62, null);
				}else if(col == 5){
					canvas.drawBitmap(bmp[4],i*80,(j-1)*60 + 62, null);
				}
			}
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		init = S_STANDBY;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		thread = null;
	}
	
	private static String num2str(int num, int len){
		String str = " " + num;
		while(str.length()<len) str = "0" + str;
		return str;
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		gestureDetector.onTouchEvent(event);
		
		return true;	
	}

	@Override
	public boolean onDoubleTap(MotionEvent event) {
		// TODO Auto-generated method stub	
		if(scene == S_PLAY){
			stage.rotate();
			drawCanvas();
		}
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent event) {
		// TODO Auto-generated method stub
		if (scene == S_STANDBY){
			init = S_PLAY;
		}else if(scene == S_PLAY){
			float  X = event.getX();
			stage.move(X);
			drawCanvas();
		}else if(scene == S_GAMEOVER){
			dialog();

		}else{
			init = S_STANDBY;
		}
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		if (velocityY > 0) {
			stage.puyoDown();
			drawCanvas();
		}
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		init = S_GAMEOVER;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public void dialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder((Activity)myContext);  
        alert.setTitle("GAME OVER");  
        alert.setMessage("ONE MORE?");  
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener(){  
            public void onClick(DialogInterface dialog, int which) {  
                //Yesボタンが押された時の処理  
            	stage = new Stage();
    			init = S_STANDBY;
            }});  
        alert.setNegativeButton("No", new DialogInterface.OnClickListener(){  
            public void onClick(DialogInterface dialog, int which) {  
                //Noボタンが押された時の処理  
    			((Activity)myContext).finish();
            }});  
        alert.show();  
    }    
}
