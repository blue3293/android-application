package com.example.antivirus;

public class Stage {
	private static final int XMax = 6;		//行のMax
	private static final int YMax = 13;	//列のMax
	private int  field[][];	//ぷよぷよの色とかの配置
	private int deleteArray[][];	//削除用配列 
	private int sequenceNum;	//連続色個数 
	private int checkColor;	//チェック用に色を格納する変数
	private int deleteNum=0;	//連鎖回数
	private boolean deletingFlag = false;	//消してる途中
	private boolean result = true;	//試合終了ですよ
	private Puyo puyo;	//落ちてくるぷよ
	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	private int score;

	public boolean deleteFlag; //ぷよを消したかどうかの判定

	enum Direction {UP, DOWN, LEFT, RIGHT};
	
	//コンストラクタ
	public Stage(){
		field = new int[XMax][YMax];
		for(int i=0;i<XMax;i++)
			for(int j=0;j<YMax;j++)
				field[i][j] = 0;
		deleteFlag=false;
	}

	//落ちる1ステップ
	public void step(){
		boolean flag = true;
		for(int i=0;i<XMax;i++)
			for(int j=0;j<YMax;j++)
				if(field[i][j] == 1){
					flag = false;
				}
		if(flag == true){
			if(field[3][1]==0){
//				if(checkFalling() == false){
				puyo = new Puyo();
				setPuyo();
//				}
			}else{
				result = false;
			}
		}else{
			int[] locate = puyo.getLocate(); //{一つ目のx,一つ目のy,二つ目のx,二つ目のy}
			for(int i=0;i<6;i++)
				for(int j=12;j>0;j--){
					if(field[i][j] == 0){
						field[i][j] = field[i][j-1];
						field[i][j-1] = 0;
					}else if(field[i][j] == 1){
						int col = 2;
						if(locate[0] == i && locate[1] == j)
							col = puyo.getAColor();
						else if(locate[2] == i && locate[3] == j)
							col = puyo.getBColor();
						field[i][j] = col;
					}
				}
			
			if(locate[1] != 12 && locate[3] != 12){
				puyo.aFall();
				puyo.bFall();
			}
//			setPuyo();
		}
		boolean flaga = true;
		for(int i=0;i<XMax;i++)
			for(int j=0;j<YMax;j++)
				if(field[i][j] == 1)
					flaga = false;
		if(flaga == true)
			puyoDelete();
	}
	
	//メソッドにする必要なし?
	private void setPuyo(){
		for(int i=0;i<XMax;i++)
			for(int j=0;j<YMax;j++)
				if(field[i][j] == 1)
					field[i][j] = 0;
		field[puyo.getLocate()[0]][puyo.getLocate()[1]] = 1;
		field[puyo.getLocate()[2]][puyo.getLocate()[3]] = 1;
	}
	
	//左右の移動
	public void move(float touchX){
		int direction;
		int[] locate = puyo.getLocate(); //{一つ目のx,一つ目のy,二つ目のx,二つ目のy}
		if(field[locate[0]][locate[1]] == 1 && field[locate[2]][locate[3]] == 1){
			if(locate[0]*80 + 40 < touchX){
				direction = RIGHT;
			}else{
				direction = LEFT;
			}
			for(int i=0;i<XMax;i++)
				for(int j=0;j<YMax;j++)
					if(field[i][j] == 1){
						field[i][j] = 0;
					}
			if(direction == RIGHT){
				if(locate[0] < XMax-1 && locate[2] < XMax-1)
					if(field[locate[0]+1][locate[1]] == 0 && field[locate[2]+1][locate[3]] == 0)
						puyo.aRight();
				if(locate[2] < XMax-1 && locate[0] < XMax-1)
					if(field[locate[2]+1][locate[3]] == 0 && field[locate[0]+1][locate[1]] == 0)
						puyo.bRight();
			}
			if(direction == LEFT){
				if(locate[0] > 0 && locate[2] > 0)
					if(field[locate[0]-1][locate[1]] == 0 && field[locate[2]-1][locate[3]] == 0)
						puyo.aLeft();
				if(locate[2] > 0 && locate[0] > 0)
					if(field[locate[2]-1][locate[3]] == 0 && field[locate[0]-1][locate[3]] == 0)
						puyo.bLeft();
			}
			setPuyo();
		}
	}
	
	//回転する
	public void rotate(){
		int[] locate = puyo.getLocate(); //{一つ目のx,一つ目のy,二つ目のx,二つ目のy}
		if(field[locate[0]][locate[1]] == 1 && field[locate[2]][locate[3]] == 1){
			if(locate[1] == locate[3]){ //横の場合
				if(locate[1] < YMax-1){
					if(locate[0] > locate[2]){
					if(field[locate[0]][locate[1]+1] == 0)
						puyo.setLocate(1, locate[0], locate[1]+1);
					}else if(locate[0] < locate[2]){
						if(locate[1] > 0 && field[locate[0]][locate[1]-1] == 0)
							puyo.setLocate(1, locate[0], locate[1]-1);
					}
				}else if(locate[1] > 0){
					if(locate[0] > locate[2]){
						if(field[locate[0]][locate[1]-1] == 0)
							puyo.setLocate(1, locate[0], locate[1]-1);
					}else if(locate[0] < locate[2]){
						if(locate[1] < YMax-1 && field[locate[0]][locate[1]+1] == 0)
							puyo.setLocate(1, locate[0], locate[1]+1);
					}
				}			
			}else if(locate[0] == locate[2]){ //縦の場合
				if(locate[0] < XMax-1){
					if(locate[1] < locate[3]){
						if(field[locate[0]+1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]+1, locate[1]);
						else if(locate[0] > 0 && field[locate[0]-1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]-1, locate[1]);
						else if(field[locate[0]][locate[1]-1] == 0)
							puyo.setLocate(1, locate[0], locate[1]-1);
					}else if(locate[1] > locate[3]){
						if(locate[0] > 0 && field[locate[0]-1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]-1, locate[1]);
						else if(locate[0] > 0 && field[locate[0]+1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]+1, locate[1]);
						else if(field[locate[0]][locate[1]+1] == 0)
							puyo.setLocate(1, locate[0], locate[1]+1);
					}
				}else if(locate[0] > 0){
					if(locate[1] > locate[3]){
						if(field[locate[0]-1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]-1, locate[1]);
						else if(locate[0] < XMax-1 && field[locate[0]+1][locate[1]] == 0)
							puyo.setLocate(1, locate[0]+1, locate[1]);
						else if(locate[1] < YMax-1 && field[locate[0]][locate[1]+1] == 0)
							puyo.setLocate(1, locate[0], locate[1]+1);
					}else if(locate[1] < locate[3]){
						if(locate[1] > 0 && field[locate[0]][locate[1]-1] == 0)
							puyo.setLocate(1, locate[0], locate[1]-1);
					}
				}
			}
			setPuyo();
		}
	}
	
	//なんか落ちてる
	private boolean checkFalling(){
		boolean checkFlag = false;
		for(int i=0; i<XMax; i++)
			for(int j=YMax-1; j>0; j--){
				if(field[i][j] == 0){
					for(int x=i-1; x>=0; x--){
						if(field[i][x]!= 0){
							checkFlag = true;
							break;
						}
					}
				}
			}
		return checkFlag;
	}
	
	public int[][] getField(){
		return field;
	}
	
	public boolean getResult(){
		return result;
	}
	
	public int getScore(){
		return score;
	}
	
	public int getColor(int x, int y){
		int[] locate = puyo.getLocate(); //{一つ目のx,一つ目のy,二つ目のx,二つ目のy}
		if(locate[0] == x && locate[1] == y){
			return puyo.getAColor();
		}else if(locate[2] == x && locate[3] == y){
			return puyo.getBColor();
		}
		return 1;
	}
	
	//以下コピペ
	//ぷよぷよの削除
	public void puyoDelete(){
		for(int i=0;i<XMax;i++){
			for(int j=0;j<YMax;j++){
				deletMain(i,j);
			}
		}	
	}

	//削除メイン処理
	private void deletMain(int i, int j) {
		//連続している色のカウントリセット
		sequenceNum = 0;

		//ぷよがなければチェックしない。
		if(field[i][j]==0) return;
		
		//チェックカラーのセット
		checkColor = field[i][j];
		
		//削除チェック配列の初期化
		deleteArray = new int[XMax][YMax];
		for(int x=0; x<XMax; x++){
			for(int y=0; y<YMax; y++){
				deleteArray[x][y]=0;
			}
		}

		//削除チェック
		deleteSub(i,j,Direction.RIGHT);

		//隣り合ったぷよぷよが４つ以上ある場合に消す。
		if(sequenceNum > 3){
			//チェック用配列が１以上の場所にあるぷよを消す
			for(int l=0; l<XMax; l++){
				for(int m=0; m<YMax; m++){
					//削除対象かのチェック
					if(deleteArray[l][m]>0){
						//削除対象であればスペースを代入
						field[l][m] = 0;
						score++;
					}
				}
		
			}
		}
	}

	//削除サブ処理
	private void deleteSub(int i, int j, Stage.Direction direction) {

		//一度チェックしたかの確認
		if(deleteArray[i][j]!=0){
			return;
		}
		
		//色が同じかどうかのチェック
		if(checkColor==field[i][j]){
			sequenceNum++;
		}else{
			return;
		}
				
		//一度チェックしたかの確認
		if(deleteArray[i][j]!=0){
			return;
		}
		else{
			deleteArray[i][j]++;
		}
		
		//右のチェック
		if(i<XMax-1 && direction!=Direction.LEFT){
			deleteSub(i+1, j, Direction.RIGHT);
		}

		//下のチェック
		if(j<YMax-1){
			deleteSub(i, j+1, Direction.DOWN);
		}

		//左のチェック
		if(i>0 && direction!=Direction.RIGHT){
			deleteSub(i-1, j, Direction.LEFT);
		}
		
		return;
	}

	//ぷよぷよを落とす
	public void puyoDown() {
		int[] locate = puyo.getLocate(); //{一つ目のx,一つ目のy,二つ目のx,二つ目のy}
		if(field[locate[0]][locate[1]] == 1 && field[locate[2]][locate[3]] == 1){
			for(int j=YMax-1; j>0; j--){
				if(locate[1] == locate[3]){
					boolean flag = true;
					if(field[locate[0]][j] == 0){
						puyo.setLocate(0, locate[0], j);
						if(flag == false)
							break;
						else	flag = false;
					}
					if(field[locate[2]][j] == 0){
						puyo.setLocate(1, locate[2], j);
						if(flag == false)
							break;
						else	flag = false;
					}
				}else if(locate[1] > locate[3]){
					if(field[locate[0]][j] == 0){
						puyo.setLocate(1, locate[2], j-1);
						puyo.setLocate(0, locate[2], j);
						break;
					}
				}else if(locate[1] < locate[3]){
					if(field[locate[0]][j] == 0){
						puyo.setLocate(1, locate[2], j);
						puyo.setLocate(0, locate[2], j-1);
						break;
					}
				}
			}
			setPuyo();
		}
	}
	
}
