package com.example.antivirus;

import android.os.Bundle;
import android.app.Activity;
import android.view.Window;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class PracticeActivity extends Activity implements View.OnClickListener{
	private final static int WC = LinearLayout.LayoutParams.WRAP_CONTENT;
	private Spinner spinner;
	
	@Override
	public void onCreate(Bundle bundle){
		super.onCreate(bundle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setResult(Activity.RESULT_CANCELED);
		LinearLayout layout = new LinearLayout(this);
		layout.setBackgroundColor(Color.rgb(255, 255, 255));
		layout.setOrientation(LinearLayout.VERTICAL);
		setContentView(layout);
		
		ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		String[] strs = {"easy","normal","hard","extra"};
		for(int i=0;i<strs.length;i++) adapter.add(strs[i]);
		spinner = new Spinner(this);
		spinner.setAdapter(adapter);
		spinner.setSelection(0);
		spinner.setLayoutParams(new LinearLayout.LayoutParams(WC,WC));
		layout.addView(spinner);
		
		layout.addView(makeButton("プレイ","play"));
		layout.addView(makeButton("戻る","back"));
	}
	
	private Button makeButton(String text, String tag){
		Button button = new Button(this);
		button.setText(text);
		button.setTag(tag);
		button.setOnClickListener(this);
		button.setLayoutParams(new LinearLayout.LayoutParams(WC,WC));
		return button;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String tag = (String)v.getTag();
		if(tag == "play"){
			Intent intent = new Intent(this,com.example.antivirus.GameActivity.class);
			intent.putExtra("level", (String)spinner.getSelectedItem());
			startActivityForResult(intent,0);
			finish();
		}else if(tag == "back"){
			Intent intent = new Intent(this,com.example.antivirus.MainActivity.class);
			startActivityForResult(intent,0);
			finish();
		}
	}
	
}
