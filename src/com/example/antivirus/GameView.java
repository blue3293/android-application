package com.example.antivirus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback,Runnable,GestureDetector.OnDoubleTapListener, OnGestureListener{
	private GestureDetector gestureDetector;
	private final static int S_STANDBY=0,
			S_PLAY = 1,
			S_CLEAR = 2,
			S_GAMEOVER = 3;
	
	private int[][] status = new int[6][13];
	
	private SurfaceHolder holder;
	private Thread thread;
	private Bitmap[] bmp = new Bitmap[3];
	
	private int scene = S_STANDBY;
	private int init = S_STANDBY;
	private String message = null;
	private int score = 0;
	
	public GameView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		bmp[0] = readBitmap(context, "test2");
		bmp[1] = readBitmap(context, "jb_4");
		bmp[2] = readBitmap(context, "jb_5");
		
		gestureDetector = new GestureDetector(context,this);
		holder = getHolder();
		holder.addCallback(this);
		holder.setFixedSize(480, 800);
	}

	private Bitmap readBitmap(Context context, String name) {
		// TODO Auto-generated method stub
		int resID = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
		return BitmapFactory.decodeResource(context.getResources(), resID);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Canvas canvas;
		Paint paint = new Paint();
		
		while(thread != null){
			if(init >= 0){
				scene = init;
				init = -1;
				if(scene == S_STANDBY){
					message = "Touch Start";
					for(int i=0;i<6;i++)
						for(int j=0;j<13;j++)
							status[i][j] = 0;
				}else if (scene == S_PLAY){
					message = null;
				}else if (scene == S_CLEAR){
					message = "Clear";
				}else if(scene == S_GAMEOVER){
					message = "Game Over";
					
				}
			}
			if (scene == S_PLAY){
				if (move() == false)
					init = S_GAMEOVER;
			}
			canvas = holder.lockCanvas();
			canvas.drawBitmap(bmp[0], 0, 0, null);
			
			paint.setColor(Color.WHITE);
			paint.setTextSize(30);
			canvas.drawText("SCORE:" + num2str(score,4), 10, 40, paint);
			
			for(int i=0;i<6;i++)
				for(int j=1;j<13;j++){
					if(status[i][j] != 0){
						canvas.drawBitmap(bmp[1],i*80,(j-1)*60 + 62, null);
					}
				}
			if(message != null){
				canvas.drawText(message, (480-paint.measureText(message))/2, 450, paint);
			}
			holder.unlockCanvasAndPost(canvas);
			try{
				Thread.sleep(300);
			}catch(Exception e){
				
			}
		}
	}
	public int Count(int x,int y,int n)	{
	    if(x+1<6 && status[x+1][y]==2) Count(x+1,y,n++);
	    if(y+1<13 && status[x][y+1]==2) Count(x,y+1,n++);
	    if(x-1>=0 && status[x-1][y]==2) Count(x-1,y,n++);
	    if(y-1>=0 && status[x][y-1]==2) Count(x,y-1,n++);
	    return n;
	}
	private void checkDelete() {
		// TODO Auto-generated method stub
		int[][] deleteArray = new int[6][13];
		for(int i=0;i<6;i++)
			for(int j=0;j<13;j++)
				deleteArray[i][j] = 1;
		for(int i=0;i<6;i++)
			for(int j=0;j<13;j++)
				if(status[i][j] == 2)
					if(Count(i,j,0) > 4)
						deleteArray[i][j] = 0;
		for(int i=0;i<6;i++)
			for(int j=0;j<13;j++)
				if(deleteArray[i][j] == 0)
					status[i][j] = 0;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		init = S_STANDBY;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		thread = null;
	}
	
	private static String num2str(int num, int len){
		String str = " " + num;
		while(str.length()<len) str = "0" + str;
		return str;
	}
	private boolean move(){
		int[] new_status  = new int[6]; 
		for(int i=0;i<6;i++)
			new_status[i] = 0;
		int mov = 0;
		boolean result = true;
		for(int i=0;i<6;i++)
			for(int j=0;j<13;j++){
				if(status[i][j] == 1 || status[i][j] == 3){
					new_status[i] = 1;
				}
				if(new_status[i] == 1 && status[i][j] == 0){
					new_status[i] = 2;
					mov++;
				}
			}
		if(mov == 0){
			if(status[3][0] == 0){
				status[3][0] = 1;
				status[3][1] = 3;
			}else
				result = false;
		}
		for(int i=0;i<6;i++)
				for(int j=12;j>0;j--){
					boolean f = true;
					if(status[i][j] == 0){
						status[i][j] = status[i][j-1];
						status[i][j-1] = 0;
						f = false;
					}
					if(f==true){
						status[i][j] = 2;
					}
				}
		return result;
	}
	
	
	public void cycle(int i, int j){
		if (status[i-1][j] == 3){
			status[i-1][j] = 0;
			status[i][j+1] = 3;
		}else if (status[i][j+1] == 3){
			if(status[i-1][j] != 2){
				status[i][j+1] = 0;
				status[i-1][j] = 3;
			}
		}else if (status[i+1][j] == 3){
			if(status[i][j+1] != 2){
				status[i+1][j] = 0;
				status[i][j+1] = 3;
			}
		}else if (status[i][j-1] == 3){
			if(status[i+1][j] != 2){
				status[i][j-1] = 0;
				status[i+1][j] = 3;
			}
		}
		
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		gestureDetector.onTouchEvent(event);
		
		return true;	
	}

	@Override
	public boolean onDoubleTap(MotionEvent event) {
		// TODO Auto-generated method stub
//		if (scene == S_PLAY){
//			for(int i=1;i<5;i++)
//				for(int j=1;j<12;j++){
//					if(status[i][j] == 1){
//						cycle(i,j);
//					}
//				}
		
//		}
		
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent event) {
		// TODO Auto-generated method stub
		if (scene == S_STANDBY){
			init = S_PLAY;
		}else if(scene == S_PLAY){
			float  X = event.getX();
			
			for(int i=0;i<6;i++)
				for(int j=0;j<13;j++){
					if(status[i][j] == 1 || status[i][j] == 3){
						if(i*80 + 40 < X){
							if(i != 5 && status[i+1][j] != 2){
								status[i+1][j] = 1;
								status[i][j] = 0;
							}
						}else if(i*80 + 40 > X){
							if(i != 0 && status[i-1][j] != 2){
								status[i-1][j] = 1;
								status[i][j] = 0;
							}
					}
				}
			}
			
		}else{
			init = S_STANDBY;

		}
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	

}
