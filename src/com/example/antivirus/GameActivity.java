package com.example.antivirus;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Window;
import android.view.WindowManager;

public class GameActivity extends Activity{
	@Override
	public void onCreate(Bundle bundle){
		Intent intent = getIntent();
		String level = intent.getStringExtra("level");
		super.onCreate(bundle);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(new GameView2(this, level));
	}

}
