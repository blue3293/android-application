package com.example.antivirus;

import java.util.Random;

public class Puyo {
	private int state;
	private int aColor;
	private int bColor;
	private int ax;
	private int ay;
	private int bx;
	private int by;
	
	public Puyo(){
		Random r = new Random();
		
		state = 0;
		aColor = r.nextInt(4) + 2;
		bColor = r.nextInt(4) + 2;
		ax = 3;
		ay = 0;
		bx = 3;
		by = 1;
	}
	public int[] getLocate(){
		int[] locate = {ax,ay,bx,by};
		return locate;
	}
	public void setLocate(int which,int x,int y){
		if(which == 0){
			ax = x;
			ay = y;
		}else if (which ==1){
			bx = x;
			by = y;
		}
	}
	public void aFall(){
		ay++;
	}
	public void bFall(){
		by++;
	}
	public void aRight(){
		ax++;
	}
	public void bRight(){
		bx++;
	}
	public void aLeft(){
		ax--;
	}
	public void bLeft(){
		bx--;
	}
	public int getAColor(){
		return aColor;
	}
	public int getBColor(){
		return bColor;
	}
	public int getState(){
		return state;
	}
	public void setState(int s){
		state = s;
	}
}
